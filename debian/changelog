quitcount (3.1.4-4) unstable; urgency=medium

  * Add patch to update appdata XML
  * Set forwarded status of all patches
  * Update Standards-Version to 4.7.0 with no other changes

 -- Ricardo Mones <mones@debian.org>  Sun, 27 Oct 2024 15:42:48 +0100

quitcount (3.1.4-3) unstable; urgency=medium

  * Team upload.
  * Make Debian Med team maintainer
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 06 Dec 2020 16:56:43 +0100

quitcount (3.1.4-2) unstable; urgency=high

  [ Jeremy Bicha ]
  * Drop obsolete Build-Depends on libunique-dev (Closes: #947515)

  [ Ricardo Mones ]
  * Fix appdata license (thanks lintian)
  * Update Standards-Version to 4.4.1 (no other changes)
  * Fix autotools-pkg-config-macro-not-cross-compilation-safe

 -- Ricardo Mones <mones@debian.org>  Sat, 28 Dec 2019 13:29:49 +0100

quitcount (3.1.4-1) unstable; urgency=medium

  * New upstream release
    - Remove patches already in this release
    - Update noautostart patch
  * debian/install
    - Install AppStream metadata to current location
  * debian/patches/noappdata.patch
    - Add patch to remove appdata targets
  * debian/copyright
    - Update URI protocol and years range
  * debian/rules, debian/control, debian/compat
    - Bump debhelper compat level to 12
  * debian/control
    - Update Standards-Version to 4.3.0

 -- Ricardo Mones <mones@debian.org>  Wed, 09 Jan 2019 00:53:31 +0100

quitcount (3.1.3-4) unstable; urgency=medium

  * debian/control
  - Update Vcs-* URLs
  - Update Standards-Version (no other changes)
  * debian/control, debian/compat
  - Set debhelper compat level to 11
  * debian/rules, debian/control
  - Enable all hardening options and autoreconf
  * debian/patches/appdata.patch
  - Add patch to upgrade AppData format

 -- Ricardo Mones <mones@debian.org>  Tue, 23 Oct 2018 01:23:37 +0200

quitcount (3.1.3-3) unstable; urgency=medium

  * debian/patches/noautostart.patch
  - Disable global autostart of desktop file (Closes: #739037)
  * debian/menu
  - Removed to implement tech-ctte resolution on #741573
  * debian/control
  - Standards-Version: bump to 3.9.8 (no other changes)
  - Vcs-*: updated to use https URLs
  * debian/README.Debian
  - Notes about automatic starting removal and how to keep it.

 -- Ricardo Mones <mones@debian.org>  Wed, 02 Nov 2016 18:49:59 +0100

quitcount (3.1.3-2) unstable; urgency=medium

  * Fix changelog entry removed in previous upload by mistake

 -- Ricardo Mones <mones@debian.org>  Wed, 17 Sep 2014 15:38:18 +0200

quitcount (3.1.3-1) unstable; urgency=medium

  * New upstream version
  * debian/patches/keywords.patch
  - Regenerated to cope with upstream changes

 -- Ricardo Mones <mones@debian.org>  Wed, 17 Sep 2014 10:36:43 +0200

quitcount (3.0-1) unstable; urgency=medium

  * New upstream version

 -- Ricardo Mones <mones@debian.org>  Sun, 15 Dec 2013 23:24:20 +0100

quitcount (2.0-3) unstable; urgency=low

  * debian/rules
  - Auto-update config.{sub,guess} (Closes: #727495)
  * debian/control
  - Standards-Version to 3.9.5 (no other changes)

 -- Ricardo Mones <mones@debian.org>  Mon, 28 Oct 2013 19:56:41 +0100

quitcount (2.0-2) unstable; urgency=low

  * debian/control, debian/compat
  - Update Vcs-* headers to use the canonical hostname
  - Bump debhelper compat level to 9 to enable hardening
  - Bump Standards-Version to 3.9.4 (no other changes)
  * debian/copyright
  - Update Format: URL and year
  * debian/patches/keywords.patch
  - Add Keywords to desktop file (thanks lintian!)

 -- Ricardo Mones <mones@debian.org>  Mon, 23 Sep 2013 14:31:18 +0200

quitcount (2.0-1) unstable; urgency=low

  * New upstream release
  * debian/control
  - This release migrates to GTK+3, updated Build-Depends

 -- Ricardo Mones <mones@debian.org>  Mon, 09 Jan 2012 20:49:59 +0100

quitcount (1.9-1) unstable; urgency=low

  * New upstream release
  * debian/compat, debian/control
  - Bump debhelper compatibility level to 8

 -- Ricardo Mones <mones@debian.org>  Thu, 05 Jan 2012 17:42:31 +0100

quitcount (1.8-2) unstable; urgency=low

  * debian/control
  - Bump Standards-Version to 3.9.2 (no other changes)
  - Fix short description to make lintian happy
  - Development moved to git.debian.org: fixed Vcs-* headers
  * debian/copyright
  - Reviewed to include src/*.svg and written in DEP5 format

 -- Ricardo Mones <mones@debian.org>  Sun, 21 Aug 2011 17:09:53 +0200

quitcount (1.8-1) unstable; urgency=low

  * New upstream release (Closes: #604804)
  * debian/control
  - Bump Standards-Version to 3.9.1 (no other changes)
  - Changed fields to follow Debian Med Group policy: Section, Priority
  - Added Vcs-* headers with current Subversion repository
  * debian/patches
  - Removed, the single patch there is already included upstream

 -- Ricardo Mones <mones@debian.org>  Fri, 10 Dec 2010 17:50:55 +0100

quitcount (1.7-1) unstable; urgency=low

  * New upstream release
  * Switch to dpkg-source 3.0 (quilt) format
  * debian/control
  - Updated Standards-Version to 3.8.4, no other changes
  * patches/fix-desktop-file.patch, patches/series
  - Fix deprecaded and non-standard keys in desktop file

 -- Ricardo Mones <mones@debian.org>  Sun, 20 Jun 2010 19:57:11 +0200

quitcount (1.6-1) unstable; urgency=low

  * New upstream release.
  * debian/control
  - Updated Standards-Version to latest, no other changes

 -- Ricardo Mones <mones@debian.org>  Mon, 21 Sep 2009 01:47:43 +0200

quitcount (1.5-1) unstable; urgency=low

  * Initial release (Closes: #537886)

 -- Ricardo Mones <mones@debian.org>  Tue, 21 Jul 2009 18:33:45 +0200
